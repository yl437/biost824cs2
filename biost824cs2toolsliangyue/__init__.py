"""This package is designed for calculating the number of mapped high-throughput sequencing data reads to the barcode(reference). The function generate_result would be used directly to the fasta and fasq files and returns a pandas dataframe that contains the number of counts, the reference barcode, and the proportion of mapped reads for each reference
Functions:
    convert_todict:
        Args:
            the fasta file
        Returns:
            fasta dictionary generated based on the fasta file
    phred2prob:
        Args:
            phred score
        Returns:
            the calculated probability based on the phred score
    read_ref_probs:
        This function takes the single read, single reference, and corresponding error probability and returns the 
        total product of probability(likelihood)
        Args:
            first: specific read
            second: specific reference
            third: error probability 
        Returns:
            The likelihood of their similarity between read and reference
    
    hamming:
        Calculate the hamming distance between two strings
        Args: 
            first: one specific read
            second: one specific ref
            third: threshold
        Returns:
            Hamming distance between one specific read and one specific reference
    
    hamming_filter:
        This function takes a read and a reference and gives wether it is in the threshold we set
        Args: 
            first: one specific read
            second: one specific ref
            third: threshold
        Returns:
            boolean variable whether the distance between read and ref is less than the threshold

    find_max_key:
        return the corresponding maximum key and values based on the values of the dictionary
        Args:
            Dictionary contains the keys and values
        Returns:
            The key whose the value is the largest in values of dictionary

    process_reads_ref:
        This is the major function of the computation. It takes the fastqfile and fastafile, and 
        the threshold we want to have, and then generate the corresponding reference that max the likelihood
        for each read

        Model:
        It uses LOOPs for each reads and search the most similar barcode in the reference library.
        It first filter out the most dissmilar barcodes for each reads by using Hamming Distances. 
        The default threshold for hamming distance is 2.
        It second calculate the maximized likelihood for each reference for each read. Then we take
        the reference for each specific read that maximized the probability(likelihood) based phred 
        score.

        Args:
            first: fastqfile
            second: fastafile
            third: threshold for hamming distances
        Returns:
            Dictionary: it takes each index of reads as the key of the dictionary, and corresponding 
            maximized probability reference and its probability(I put them into tuples) as the values.

    convert_data_frame:
        Convert the result from above function and generate it to a dataframe
        Args:
            Dictionary:
            Generated dictionary result from the error probability model using loop(it generates a dictionary)
        Returns:
            Pandas DataFrame
            It returns the summary data frame from the dictionary read_ref_maxprob
    
    unmapped_ref: 
        Figure out the rest of the refereneces that does not have any mapped reads
        Args:
            First: The generated dataframe of result
            Second: The generated fasta dicitonary
        Returns:
            It returns the complete data frame of the result we want that includes
            all of references that don't have any mapped reads
    
    generate_result:
        This is the final function that takes all steps. It calls all of functions we mentioned
        above and generate the final dataframe that contains the information we want It processed the fastqfile, 
        fastafile and the threshold, and compute the maximized likelihood for each read and each reference, and 
        generate the number of counts for each reference
        
        Parameters:
        -----------
        First: fasta file that contains all of barcodes in the library(referenrces)
        Second: fastq file that contains all of reads information in the library(read result from the machine)
        
        Returns:
        -------
        Data frame:
            The returned data frame contains the all of the references code, and its corresponding number of mapped reads based on error 
            probability model. It also contains a column that shows the propotition of mapped reads of each reference given total number of
            mapped reads.
    
    generate_plot:
        Args:
            The result generated from 'generate_result'
        Returns:
            plot out the each reference and its
            corresponding counts of mapped reads
"""


from Bio import SeqIO
import math
#import statistics
import numpy as np
import collections
import hashlib
import itertools


# In[6]:

#this works for any fasta fastq file
# fastafile = "*.fasta"
# fastqfile = "*.fastq"

def convert_todict(fastafile):
    """Convert the fasta file to a python dictionary
    Args:
        the fasta file
    Returns:
        fasta dictionary generated based on the fasta file"""
    fastadict = {}
    for idx, myref in enumerate(SeqIO.parse(fastafile, "fasta")):
        # The description will used as key and the sequence string will be the corresponding value
        fastadict[myref.description] = str(myref.seq)
    return fastadict

# # Calculate error probability

# In[66]:


def phred2prob(phred):
    """Calculate the probability using the phred score
    Args:
        phred score
    Returns:
        the calculated probability based on the phred score"""
    # calculate 10^{-phred/10}
    prob = list(map(lambda x: math.pow(10.0, -x/10.0), phred))
    return(prob)


# In[67]:


def read_ref_probs(read, ref, epsilon):
    """This function takes the single read, single reference, and corresponding error probability and returns the 
    total product of probability(likelihood)
    Args:
        first: specific read
        second: specific reference
        third: error probability 
    Returns:
        The likelihood of their similarity between read and reference"""
    product_prob = []
    for i in zip(read, ref, epsilon):
        if i[0] == i[1]:
            product_prob.append(1-i[2])
        elif i[0] != i[1]:
            product_prob.append(i[2]/3)
    return np.product(product_prob)


# # Hamming distance:

# In[80]:


def hamming(x, y):
    """Calculate the hamming distance between two strings
    Args: 
        first: one specific read
        second: one specific ref
        third: threshold
    Returns:
        Hamming distance between one specific read and one specific reference"""
    assert len(x) == len(y)
    return sum(i != j for i, j in zip(x, y))


# # Write a function of filter.

# In[81]:


def hamming_filter(read, ref, threshold):
    """This function takes a read and a reference and gives wether it is in the threshold we set
    Args: 
        first: one specific read
        second: one specific ref
        third: threshold
    Returns:
        boolean variable whether the distance between read and ref is less than the threshold"""
    return hamming(read, ref) < threshold
    


# In[95]:


def find_max_key(dict):
    """return the corresponding maximum key and values based on the values of the dictionary
    Args:
        Dictionary contains the keys and values
    Returns:
        The key whose the value is the largest in values of dictionary
    """
    return max(dict, key=lambda k: dict[k])     


# In[267]:


def process_reads_ref(fastqfile, fastafile, threshold):
    """This is the major function of the computation. It takes the fastqfile and fastafile, and 
    the threshold we want to have, and then generate the corresponding reference that max the likelihood
    for each read

    Model:
    It uses LOOPs for each reads and search the most similar barcode in the reference library.
    It first filter out the most dissmilar barcodes for each reads by using Hamming Distances. 
    The default threshold for hamming distance is 2.
    It second calculate the maximized likelihood for each reference for each read. Then we take
    the reference for each specific read that maximized the probability(likelihood) based phred 
    score.

    Args:
        first: fastqfile
        second: fastafile
        third: threshold for hamming distances
    Returns:
        Dictionary: it takes each index of reads as the key of the dictionary, and corresponding 
        maximized probability reference and its probability(I put them into tuples) as the values.
            """
    read_ref_maxprob = {}
    for idx_read, myread in enumerate(SeqIO.parse(fastqfile, "fastq")):
        myref_maxprob = []
        for idx_ref, myref in enumerate(SeqIO.parse(fastafile, "fasta")):
            if hamming_filter(myread.seq, myref.seq, threshold):
                current_prob = phred2prob(myread.letter_annotations['phred_quality'])
                myref_maxprob.append((str(myref.seq), read_ref_probs(myread.seq, myref.seq, current_prob)))
        if len(myref_maxprob) == 0:
            myref_maxprob.append(("No ref", 0))
        read_ref_maxprob[idx_read] = (str(myread.seq), max(myref_maxprob, key = lambda x:x[1]))
    return read_ref_maxprob


# In[268]:


#read_ref_maxprob = process_reads_ref(fastqfile, fastafile, threshold = 3)


# In[269]:


def convert_data_frame(read_ref_maxprob):
    """Convert the result from above function and generate it to a dataframe
    Args:
        Dictionary:
        Generated dictionary result from the error probability model using loop(it generates a dictionary)
    Returns:
        Pandas DataFrame
        It returns the summary data frame from the dictionary read_ref_maxprob"""
    import pandas as pd
    df = pd.DataFrame({"reads_idx":list(read_ref_maxprob.keys()), "results": list(read_ref_maxprob.values())})
    df['results_reads'] = df['results'].str[0]
    df['results_ref_prob'] = df['results'].str[1]
    df['results_ref'] = df['results_ref_prob'].str[0]
    df['results_prob'] = df['results_ref_prob'].str[1]
    
    #calculate the total number of reads that have maps to the references: 
    num = df.drop(columns = ['results', 'results_ref_prob'], axis = 1).loc[df.results_ref != "No ref"].reset_index().shape[0]
    result_df = df.groupby(['results_ref']).size().reset_index(name = "counts")
    result_df["proportion"] = result_df.counts/num
    return result_df


# In[270]:


# result_df = convert_data_frame(read_ref_maxprob)


# In[ ]:





# In[303]:


def unmapped_ref(result_df, fastadict):
    """Figure out the rest of the refereneces that does not have any mapped reads
    Args:
        First: The generated dataframe of result
        Second: The generated fasta dicitonary
    Returns:
        It returns the complete data frame of the result we want that includes
        all of references that don't have any mapped reads"""
    temp_df = result_df
    import pandas as pd
    results_ref = []
    counts = []
    proportion = []
    for i in set(fastadict.values()) - set(result_df.results_ref):
        results_ref.append(i)
        counts.append(0)
        proportion.append(0)
    temp_df = temp_df.append(pd.DataFrame({"results_ref":results_ref, "counts": counts, "proportion":proportion}))
    return temp_df


# # In[304]:


# final_result=unmapped_ref(result_df, fastadict)


# # In[302]:


# final_result


# In[ ]:

def generate_result(fastafile, fastqfile):
    """This is the final function that takes all steps. It processed the fastqfile, fastafile and the threshold, and 
    compute the maximized likelihood for each read and each reference, and generate the number of counts
    for each reference
    
    Parameters:
    -----------
    First: fasta file that contains all of barcodes in the library(referenrces)
    Second: fastq file that contains all of reads information in the library(read result from the machine)
    
    Returns:
    -------
    Data frame:
        The returned data frame contains the all of the references code, and its corresponding number of mapped reads based on error 
        probability model. It also contains a column that shows the propotition of mapped reads of each reference given total number of
        mapped reads.
    """
    read_ref_maxprob = process_reads_ref(fastqfile, fastafile, threshold = 3)
    result_df = convert_data_frame(read_ref_maxprob)
    fastadict = convert_todict(fastafile)
    final_result=unmapped_ref(result_df, fastadict)
    return final_result

def generate_plot(final_result):
    """Generate the count plots
    Args:
        The result generated from 'generate_result'
    Returns:
        plot out the each reference and its
        corresponding counts of mapped reads"""
    import matplotlib.pyplot as plt
    final_result = final_result.loc[final_result.results_ref != "No ref"]
    plt.bar(final_result.index, final_result.counts)
    plt.xlabel("Index of reads")
    plt.ylabel("Counts")
    #plt.show()
    plt.savefig("./result_plot.png")



