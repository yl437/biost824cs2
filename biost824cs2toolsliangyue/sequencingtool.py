#!/usr/bin/env python
# coding: utf-8

# In[5]:


from Bio import SeqIO
import math
#import statistics
import numpy as np
import collections
import hashlib
import itertools


# In[6]:


fastafile = "my.gen.fasta"
fastqfile = "my.gen.fastq"


# In[215]:

def convert_todict(fastafile):
    fastadict = {}
    for idx, myref in enumerate(SeqIO.parse(fastafile, "fasta")):
        # The description will used as key and the sequence string will be the corresponding value
        fastadict[myref.description] = str(myref.seq)
    return fastadict

# # Calculate error probability

# In[66]:


def phred2prob(phred):
    # calculate 10^{-phred/10}
    prob = list(map(lambda x: math.pow(10.0, -x/10.0), phred))
    return(prob)


# In[67]:


def read_ref_probs(read, ref, epsilon):
    """This function takes the single read, single reference, and corresponding error probability and returns the 
    total product of probability(likelihood)"""
    product_prob = []
    for i in zip(read, ref, epsilon):
        if i[0] == i[1]:
            product_prob.append(1-i[2])
        elif i[0] != i[1]:
            product_prob.append(i[2]/3)
    return np.product(product_prob)


# # Hamming distance:

# In[80]:


def hamming(x, y):
    """Calculate the hamming distance between two strings"""
    assert len(x) == len(y)
    return sum(i != j for i, j in zip(x, y))


# # Write a function of filter.

# In[81]:


def hamming_filter(read, ref, threshold):
    """This function takes a read and a reference and gives wether it is in the threshold we set"""
    return hamming(read, ref) < threshold
    


# In[95]:


def find_max_key(dict):
    return max(dict, key=lambda k: dict[k])     


# In[267]:


def process_reads_ref(fastqfile, fastafile, threshold):
    read_ref_maxprob = {}
    for idx_read, myread in enumerate(SeqIO.parse(fastqfile, "fastq")):
        myref_maxprob = []
        for idx_ref, myref in enumerate(SeqIO.parse(fastafile, "fasta")):
            if hamming_filter(myread.seq, myref.seq, threshold):
                current_prob = phred2prob(myread.letter_annotations['phred_quality'])
                myref_maxprob.append((str(myref.seq), read_ref_probs(myread.seq, myref.seq, current_prob)))
        if len(myref_maxprob) == 0:
            myref_maxprob.append(("No ref", 0))
        read_ref_maxprob[idx_read] = (str(myread.seq), max(myref_maxprob, key = lambda x:x[1]))
    return read_ref_maxprob


# In[268]:


#read_ref_maxprob = process_reads_ref(fastqfile, fastafile, threshold = 3)


# In[269]:


def convert_data_frame(read_ref_maxprob):
    import pandas as pd
    df = pd.DataFrame({"reads_idx":read_ref_maxprob.keys(), "results": read_ref_maxprob.values()})
    df['results_reads'] = df['results'].str[0]
    df['results_ref_prob'] = df['results'].str[1]
    df['results_ref'] = df['results_ref_prob'].str[0]
    df['results_prob'] = df['results_ref_prob'].str[1]
    
    #calculate the total number of reads that have maps to the references: 
    num = df.drop(columns = ['results', 'results_ref_prob'], axis = 1).loc[df.results_ref != "No ref"].reset_index().shape[0]
    result_df = df.groupby(['results_ref']).size().reset_index(name = "counts")
    result_df["proportion"] = result_df.counts/num
    return result_df


# In[270]:


# result_df = convert_data_frame(read_ref_maxprob)


# In[ ]:





# In[303]:


def unmapped_ref(result_df, fastadict):
    temp_df = result_df
    import pandas as pd
    results_ref = []
    counts = []
    proportion = []
    for i in set(fastadict.values()) - set(result_df.results_ref):
        results_ref.append(i)
        counts.append(0)
        proportion.append(0)
    temp_df = temp_df.append(pd.DataFrame({"results_ref":results_ref, "counts": counts, "proportion":proportion}))
    return temp_df


# # In[304]:


# final_result=unmapped_ref(result_df, fastadict)


# # In[302]:


# final_result


# In[ ]:

def generate_result(fastafile, fastqfile):
    read_ref_maxprob = process_reads_ref(fastqfile, fastafile, threshold = 3)
    result_df = convert_data_frame(read_ref_maxprob)
    final_result=unmapped_ref(result_df, fastadict)
    return final_result


