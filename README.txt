# Biost824CS2

Installing the package:
pip install git+https://gitlab.oit.duke.edu/yl437/biost824cs2.git

This package is designed for calculating the number of mapped high-throughput sequencing data reads to the barcode(reference). The function generate_result would be used directly to the fasta and fasq files and returns a pandas data-frame that contains the number of counts, the reference barcode, and the proportion of mapped reads for each reference

Modeling: The main function is the generate_result funtion and all of other helper functions 
        It uses LOOPs for each reads and search the most similar barcode in the reference library.
        It first filter out the most dissimilar barcodes for each reads by using Hamming Distances. 
        The default threshold for hamming distance is 2.
        It second calculate the maximized likelihood for each reference for each read. Then we take
        the reference for each specific read that maximized the probability(likelihood) based phred 
        score.

Timing: This code for generating the pandas data frames takes about 5 minutes to run out the result.

Functions:
    convert_todict:
        Args:
            the fasta file
        Returns:
            fasta dictionary generated based on the fasta file
    phred2prob:
        Args:
            phred score
        Returns:
            the calculated probability based on the phred score
    read_ref_probs:
        This function takes the single read, single reference, and corresponding error probability and returns the 
        total product of probability(likelihood)
        Args:
            first: specific read
            second: specific reference
            third: error probability 
        Returns:
            The likelihood of their similarity between read and reference
    
    hamming:
        Calculate the hamming distance between two strings
        Args: 
            first: one specific read
            second: one specific ref
            third: threshold
        Returns:
            Hamming distance between one specific read and one specific reference
    
    hamming_filter:
        This function takes a read and a reference and gives wether it is in the threshold we set
        Args: 
            first: one specific read
            second: one specific ref
            third: threshold
        Returns:
            boolean variable whether the distance between read and ref is less than the threshold

    find_max_key:
        return the corresponding maximum key and values based on the values of the dictionary
        Args:
            Dictionary contains the keys and values
        Returns:
            The key whose the value is the largest in values of dictionary

    process_reads_ref:
        This is the major function of the computation. It takes the fastqfile and fastafile, and 
        the threshold we want to have, and then generate the corresponding reference that max the likelihood
        for each read

        Model:
        It uses LOOPs for each reads and search the most similar barcode in the reference library.
        It first filter out the most dissmilar barcodes for each reads by using Hamming Distances. 
        The default threshold for hamming distance is 2.
        It second calculate the maximized likelihood for each reference for each read. Then we take
        the reference for each specific read that maximized the probability(likelihood) based phred 
        score.

        Args:
            first: fastqfile
            second: fastafile
            third: threshold for hamming distances
        Returns:
            Dictionary: it takes each index of reads as the key of the dictionary, and corresponding 
            maximized probability reference and its probability(I put them into tuples) as the values.

    convert_data_frame:
        Convert the result from above function and generate it to a dataframe
        Args:
            Dictionary:
            Generated dictionary result from the error probability model using loop(it generates a dictionary)
        Returns:
            Pandas DataFrame
            It returns the summary data frame from the dictionary read_ref_maxprob
    
    unmapped_ref: 
        Figure out the rest of the refereneces that does not have any mapped reads
        Args:
            First: The generated dataframe of result
            Second: The generated fasta dicitonary
        Returns:
            It returns the complete data frame of the result we want that includes
            all of references that don't have any mapped reads
    
    generate_result:
        This is the final function that takes all steps. It calls all of functions we mentioned
        above and generate the final dataframe that contains the information we want It processed the fastqfile, 
        fastafile and the threshold, and compute the maximized likelihood for each read and each reference, and 
        generate the number of counts for each reference
        
        Parameters:
        -----------
        First: fasta file that contains all of barcodes in the library(referenrces)
        Second: fastq file that contains all of reads information in the library(read result from the machine)
        
        Returns:
        -------
        Data frame:
            The returned data frame contains the all of the references code, and its corresponding number of mapped reads based on error 
            probability model. It also contains a column that shows the propotition of mapped reads of each reference given total number of
            mapped reads.
    
    generate_plot:
        Args:
            The result generated from 'generate_result'
        Returns:
            plot out the each reference and its
            corresponding counts of mapped reads