#setup file for python packages
from setuptools import setup

setup(
	name = 'biost824cs2toolsliangyue',
	url = 'https://gitlab.oit.duke.edu/yl437/biost824cs2.git',
	author = 'Lync(Yue Liang)',
	author_email = 'yue.liang1@duke.edu',
	packages = ['biost824cs2toolsliangyue'],
	package_dir = {'biost824cs2toolsliangyue': './biost824cs2toolsliangyue'},
	package_data={'biost824cs2toolsliangyue':['my.gen.fasta' ,'my.gen.fastq']},
	install_requires = ['numpy', 'biopython', 'pandas'],
	license = 'Duke',
	description = 'Tool of using fastqfile and fasta file to return the number of mapped reads for each barcode and its corresponding proportions.',
	long_description = open('README.txt', 'r').read(),
	#include_package_data=True
)